# Be aware of AWS Sandbox limits:
# https://support.linuxacademy.com/hc/en-us/articles/360025783132-What-can-I-do-with-the-Cloud-Playground-AWS-Sandbox-

provider "aws" {
  region = var.region
}

module "label" {
  source  = "git::https://github.com/cloudposse/terraform-terraform-label.git?ref=tags/0.4.0"

  namespace  = var.namespace
  name       = var.name
  stage      = var.stage
  delimiter  = var.delimiter
  attributes = var.attributes
  tags       = var.tags
}

#-----
# DATA
#-----

# You can use LinuxAcademy public VPC if you like
# https://www.terraform.io/docs/providers/aws/d/vpcs.html

data "aws_caller_identity" "this" {}

data "aws_vpcs" "public" {
  tags = {
    Network = "Public"
  }
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.9.0"

  name = module.label.id

  cidr = var.cidr

  azs             = var.availability_zones
  public_subnets  = var.public_subnets

  assign_generated_ipv6_cidr_block = var.assign_generated_ipv6_cidr_block

  enable_nat_gateway     = var.enable_nat_gateway
  single_nat_gateway     = var.single_nat_gateway
  one_nat_gateway_per_az = var.one_nat_gateway_per_az

  enable_dns_support   = var.enable_dns_support
  enable_dns_hostnames = var.enable_dns_hostnames

  vpc_tags = module.label.tags

  private_subnet_tags = {
    Tier = "private"
  }

  public_subnet_tags = {
    Tier = "public"
  }
}
